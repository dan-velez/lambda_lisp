var lamlisp_term = 
{
// cmd_mode = false on window refocus

//input : "<div id='input' contenteditable=true style="
//	+"'width:50%;height:300px;" 
//	+"float:left;overflow:auto;'></div>",

//output : "<div id='output' style="
//	+"'width:50%;height:300px;"
//	+"float:right;'></div>",

/*input : "<textarea id='input' style='resize:horizontal;width:60%;"
	+"tab-size:4;-moz-tab-size:4;'"
	+"rows=16 autofocus focus spellcheck=false></textarea>",
output : "<textarea id='output' style='resize:horizontal;width:35%;'"
	+"disabled rows=16 spellcheck=false></textarea>",
*/

input : "<textarea id='input' style='resize:vertical;width:100%;"
	+"tab-size:4;-moz-tab-size:4;'"
	+"rows=20 autofocus focus spellcheck=false></textarea><hr/>",

output : "<textarea id='output' style='resize:vertical;width:100%;'"
	+"disabled rows=4 spellcheck=false></textarea>",

lambda : String.fromCharCode(654),
input_pntr : 0,
//lang : lamlisp(), /// pointer to interpreter
cmd_mode : false,
cmd_key : 18,
tab : "\t",

str_insert : (str, str2, idx) => {
	return str.slice(0, idx+1) + str2 
		+ str.slice(idx+1, str.length);
	},

textarea_ins : (textarea, str) => {
//	inserts str at the current caret position of
//	textarea and moves caret to end of str
	if(document.selection) 
		(document.selection.createRange()).text = str;
	else {
		let start = textarea.selectionStart;
		let end = textarea.selectionEnd;
		textarea.value = textarea.value.substring(0, start) 
			+ str
			+ textarea.value.substring(
				end, textarea.value.length);
		textarea.selectionStart = start+1;
		textarea.selectionEnd = end+1;
		} 
	},

keyup : (e) => {
	switch(e.keyCode) {
		case lamlisp_term.cmd_key:
		lamlisp_term.cmd_mode = false;
		break;
		}
	},

keydown : (e) => {
//	console.log(input.selectionStart);
	console.log(e.keyCode);
	switch(e.keyCode) {
		case lamlisp_term.cmd_key:
		e.preventDefault();
		lamlisp_term.cmd_mode = true;
		break;
	
		case 79: // o
		if(lamlisp_term.cmd_mode) {
			input.selectionStart--;
			input.selectionEnd--;	
			}
		break;

		case 80: // p
		if(lamlisp_term.cmd_mode) {
			input.selectionStart++;
//			input.selectionEnd++;	
			}
		break;

		case 13: // enter
		if(lamlisp_term.cmd_mode) {
			e.preventDefault();
//			var lang = new lamlisp();
			output.value = lamlisp.run.call(lamlisp, input.value);
//			output.value = lang.run.call(lang, input.value);
//			output.value = lamlisp.run.call(lamlisp, input.value);
			}
		break;
	
		case 9: // tab
		e.preventDefault();
		lamlisp_term.textarea_ins(input, lamlisp_term.tab);
		break;

//		192 : backtick
		case 220: // backslash
		e.preventDefault();
		lamlisp_term.textarea_ins(input, lamlisp_term.lambda);
		break;

		case 219: // lbracket (9)
		e.preventDefault();
		lamlisp_term.textarea_ins(input, "(");
		break;

		case 221: // rbracket (0)
		e.preventDefault();
		lamlisp_term.textarea_ins(input, ")");
		break;
		}
	}
}

document.body.innerHTML+=lamlisp_term.input;
document.body.innerHTML+=lamlisp_term.output;
lamlisp_term.input = document.getElementById('input');
lamlisp_term.output = document.getElementById('output');

let lambda = String.fromCharCode(654);
let nub = 1;
if(nub)
lamlisp_term.input.value =
"(\n"
+"// multiple s-expressions should be enclosed in an\n"
+"// s-epxression\n\n" 

+"// atoms evaluate to themselves\n"
+"// A B C 1 2 3 4 0xFF 0011\n\n"

+"// lists do as well\n"
+"// (12.34 09.87 0xFF)\n\n"

+"// the car of a list is returned by ="+lambda+"\n"
+"// (="+lambda+" ((2 4) 6 8 10))\n\n"

+"// the cdr of a list is returned by "+lambda+"=\n"
+"// ("+lambda+"= ((2 4) 6 8 10))\n\n"

+"// the cons of two lists||atoms is returned by ="+lambda+"=\n"
+"// (="+lambda+"= (a b c) (d e f))\n"
+"// (="+lambda+"= (a e i o) u)\n"
+"// (="+lambda+"= o u)\n\n"

+"// if an atom is a function, it is called with the next\n"
+"// variable s-expressions, depending on what the function\n"
+"// requires as input.\n"
+"// ("+lambda+"= (1 2 3 4))\n"
+"// (a b c "+lambda+"= (1 2 3 4))\n\n"

+"// functions only take what they NEED to take. e.g. ="+lambda+"=\n"
+"// takes the next two s-expressions.\n"
+"// ((="+lambda+"= a e)  ("+lambda+"= (i o u))  sss)\n"
+"// ((="+lambda+"= a e)  ("+lambda+"= (i o u)   sss))\n"
+"//  (="+lambda+"= a e   ("+lambda+"= (i o u)   sss))\n"
+"// ((="+lambda+"= a e)   "+lambda+"= (i o u)   sss)\n"
+"// ((="+lambda+"= a)    ("+lambda+"= (i o u)   sss))\n\n"

+"// function combinations:\n"
+"// ((="+lambda+" "+lambda+"= (3 4 5 6)) (a b c d e))\n"
+"// (("+lambda+"= (3 4 ="+lambda+" "+lambda+"= (6 7 8))) (a b c d e))\n\n"

+"// arithmetic operators acuumulate the rest of the list, and\n"
+"// do not accept lists as arguments, unless they evaluate\n"
+"// to an atom.\n"
+"// (+ 5 10.5 (+ 25.1 30.1))\n"
+"// (1 + 3 4 5 6)\n"
+"// (1 2 (/ 64 3) 3 / 50 2 5)\n\n"

+"// the builtin "+lambda+": defines a function in the global\n"
+"// environment and can be read as 'define'.\n"
+"// ("+lambda+": fn (x y z) (+ x y (* z 2)))\n"
+"// (fn 5 10 20)\n\n"

+"// the builting ? is a coditional statement\n"
+"// if the s-expression following ? evalutes to a digit\n"
+"// that is not less than or equal to zero or a non-empty\n"
+"// list, the next s-expression is evaluated. Otherwise, the\n"
+"// expression after the next is evaluated.\n"
+"// (? 1 1 2) (? 0 1 2)\n"
+"// (? (- 1 2 3)\n"
+"//	(+ 4 5 6)\n"
+"//	(? 1\n"
+"// 		(^ 2 2 2 2)\n"
+"//		(00 1)))\n"
+")";
else lamlisp_term.input.value = "()";

lamlisp_term.input.addEventListener('keyup',
	lamlisp_term.keyup); 
lamlisp_term.input.addEventListener('keydown',
	lamlisp_term.keydown); 
lamlisp_term.input.focus(),

Array.prototype.toString = function () {
	let res = "(";
	for(let i = 0; i < this.length; i++)
		if(i < this.length-1) res+=this[i] +" ";
		else res+=this[i]+")";
	return res;
	}
