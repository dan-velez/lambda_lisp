var lamlisp = lamlisp || {};

lamlisp.lambda = String.fromCharCode(654);

lamlisp.fns = [
	["="+lamlisp.lambda+"=", function(list) { 
//		cons
		if(list.cdr == null) return list;
		return lamlisp.cons(list.car, list.cdr.car);
		}],

	["="+lamlisp.lambda, function (list) { 
//		car
// 		input is ( (list) )
		console.log("calling car on:");
		console.log(list);
		if(this.atomp(list.car)) return this.eval(list);
//			{
//			car : list.car, 
//			cdr : null
//			};
		else return this.eval(list.car).car; 
		}],

	[lamlisp.lambda+"=", function (list) {
//		cdr
		console.log("calling cdr on:");
		console.log(list);
		if(this.atomp(list.car)) return null;
		else return this.eval(list.car).cdr;
		}],

	[lamlisp.lambda+":", function (list) {
//		define a procedure
		console.log("procedure definition with list:");
		console.log(list);

//		(lambda: fn (x) x)
//		(fn (x y z) (+ x y z))
//		adds definition to env, unevaluated

		let name = list.car; // atom
		let args = list.cdr.car; // list of atoms
		let body = list.cdr.cdr; // encapsulate

		console.log("procedure details:");
		console.log(name);
		console.log(args);
		console.log(body);

		this.fns.push (
			[name, function (input) {	
				return this.eval(
					lamlisp.sub_args(
						lamlisp.zip(args, 
							lamlisp.eval(input)), 
						body));
//					sub (args) with (input) in (body)
				}]);
		return ":"+this.lambda+":";
		}],

//	conditional branch && logical operators -> T or F	
	["?", function (list) {
//		if statement

		let val = list.car;

		if(!(this.atomp(list.car))) 
			val = this.eval(list.car);

		if(list.cdr == null) return null;

		if(val != null && val != "nil" && val > 0)
			return this.eval(list.cdr.car);
		else return this.eval(list.cdr.cdr);
		}],


	["||", function (list) {
//		if statement
		return this.eval(list.car).cdr;
		}],

	["!!", function (list) {
//		if statement
		return this.eval(list.car).cdr;
		}],

	["&&", function (list) {
//		if statement
		return this.eval(list.car).cdr;
		}],

	["@", function (list) {
//		cdr
		return this.eval(list.car).cdr;
		}],

	["=", function (list) {
//		cdr
		return this.eval(list.car).cdr;
		}],

	["+", function(list) { 
		return this.arith_accum(list, "+");
		}],

	["-", function(list) { 
		return this.arith_accum(list, "-");
		}],

	["*", function(list) { 
		return this.arith_accum(list, "*");
		}],

	["/", function(list) { 
		return this.arith_accum(list, "/");
		}],

	["^", function (list) {
//		if statement
		return this.arith_accum(list, "^");
		}],

	["%", function (list) {
//		if statement
		return this.arith_accum(list, "%");
		}]
	];

lamlisp.eval_to_atom = function (sexp) {
	if(!this.atomp(sexp)) {
		let buff_eval = this.eval(sexp);
		if(this.atomp(buff_eval)) return buff_eval;
		else return 0;
		}
	else return sexp;
	}

lamlisp.cons_old = function (list) {
//	cons
//	return this.cons(list.car, list.cdr);
	console.log("consing cdr and car of: ");
	console.log(list);
	let res = {
		car : this.eval(list.car),
		cdr : this.eval(list.cdr).car
		}
	console.log("cons res:");
	console.log(res);
	return res;
	}

lamlisp.cons = function (sexp1, sexp2, eval="true") {
	console.log("consing following two sexp:");
	console.log(sexp1);
	console.log(sexp2);
	if(eval) {
		if(sexp1 != null) sexp1 = this.eval(sexp1);
		if(sexp2 != null) sexp2 = this.eval(sexp2);
		}
	let res = {
		car : sexp1,
		cdr : sexp2
		}
	console.log("cons res:");
	console.log(res);
	return res;
	}
/*

IT ->
	wed june 29 7pm

(+ 1 2 (3 4) 5)
(ʎ: ʎ+ (seq)
	(? (@ʎ seq) 
		(+ seq (addr ʎ= seq))

(ʎ: ʎ+ (seq)
	(? (. (=ʎ seq)) 
		(+ seq (addr (ʎ= seq)))
		(+ (addr (=ʎ seq)) 
			(addr (ʎ= seq)))))	

(ʎ: ʎ+ (seq)
	(? (@ʎ (=ʎ seq)) 
		(+ seq (addr (ʎ= seq)))
		(+ (addr (=ʎ seq)) 
			(addr (ʎ= seq)))))

*/

/// macro language in lisp 
// should be a lanugage seperate from lisp
// itself, maybe a C-type language
//defmacro traverse (list, fn, op="+") {
//	}
