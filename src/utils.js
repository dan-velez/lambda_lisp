lamlisp.syntax_check = function (prog) {
	let depth = 0;
	for(let i = 0; i < prog.length; i++)
		if(prog[i] == "(") ++depth;
		else if(prog[i] == ")") --depth;
	if(depth == 0) return true;
	else return null;	
	}

lamlisp.list2str = function (list) {
	if(list == null) return "()";
	if(typeof list != "object") return list;
	let res = "(";
	let buff = list.cdr;
	if(!this.atomp(list.car))
		res += ""+this.list2str(list.car)+"";
	else {
		if(buff == null) res += list.car;
		else res += list.car + " ";
		}
	if(buff != null) {
		res += this.list2str(buff);	
		buff = buff.cdr;
		} 
	return res+")";
	}

lamlisp.list2sexp= function (list) {
	if(list == null) return "nil";
	if(typeof list != "object") return list;
	let res = "";
	let buff = list.cdr;
	if(!this.atomp(list.car))
		res += "("+this.list2sexp(list.car)+") ";
	else {
		if(buff == null) res += list.car;
		else res += list.car + " ";
		}
	if(buff != null) {
		res += this.list2sexp(buff);	
		buff = buff.cdr;
		} 
	return res+"";
	}

lamlisp.rm_comments = function (prog) {
	let res = "";
	let state = "capture"; // | in comment
	for(let i = 0; i < prog.length; i++)
		switch(state) {
			case "capture":
			if(prog[i] == '/' && prog[i+1] == '/')
				state = "comment";
			else res+=prog[i];
			break;

			case "comment":
			if(prog[i] == '\n') state = "capture";
			else break;
			}
	return res;
	}

lamlisp.atomp = function (sexp) {
	return typeof sexp != "object";
	}

lamlisp.zip = function (list1, list2) {
//	returns an object with each sexp of list1 as a prop
//	with a value of sexp at matching position in list2
	console.log("zipping lists:");
	console.log(list1);
	console.log(list2);
	
	let res = {};
	let buff1 = list1;
	let buff2 = list2;
	while(buff1 != null && buff2 != null) {
		res[buff1.car] = buff2.car;
		buff1 = buff1.cdr;
		buff2 = buff2.cdr;
		}
	console.log("zip res");
	console.log(res);
	return res;
	}

lamlisp.map = function (list, fn) {
	console.log("map call with:");
	console.log(list);
	if(list == null) return null;
	else {
		let res = this.cons(fn(list.car), 
			this.map(list.cdr, fn));

		console.log("map cons:");
		console.log(res);
		return res;
		}
	}

lamlisp.append = function (sexp1, sexp2) {
	}

lamlisp.filter = function (list, fn) {

	}

lamlisp.accum = function (list, fn) {

	}

lamlisp.arith_accum = function (list, op) {
// 	arithmetic accumulate
//	returns atom
	console.log("accum recieved:");	
	console.log(list);
	let res = null;
	let buff = list;
	while(buff != null) {
		console.log("calling eval from accum with input:");
		console.log(buff.car);

		let buff_eval = this.eval_to_atom(buff.car);

		console.log("evaled to:");
		console.log(buff_eval);

		switch(op) {
			case "+":
			res += buff_eval;
			break;

			case "-" :
			if(res == null) {
				res = buff_eval;
				break;
				}
			res -= buff_eval;
			break;

			case "*" :
			res == null ? res = 1 : true;
			res *= buff_eval;
			break;

			case "/" :
			if(res == null) {
				res = buff_eval;
				break;
				}
			res /= buff_eval;
			break;

			case "%":
			if(res == null) {
				res = buff_eval;
				break;
				}
			res = res % buff_eval;	
			break;

			case "^":
			if(res == null) {
				res = buff_eval;
				break;
				}
			res = Math.pow(res, buff_eval);	
			break;		
			}

		console.log("res is now:");
		console.log(res);

		buff = buff.cdr;
		}

	console.log("list_acuum res:");
	console.log(res);

	return res;
	}

lamlisp.sub_args = function (hash, body) {
//	body is the function body
	console.log("sub_args call with body:");
	console.log(body);

	if(body == null) return null;
	if(!this.atomp(body.car))
		return this.cons(this.sub_args(hash, body.car),
			this.sub_args(hash, body.cdr), false);
	else {
		if(hash[body.car]) 
			return this.cons(hash[body.car], 
				this.sub_args(hash, body.cdr), false);
		else return this.cons(body.car, 
			this.sub_args(hash, body.cdr), false);
		}
	}

lamlisp.substitute_args = function (args, vals, body) {
//	Searches list for any occurences of atoms in list and 
//	substitutes them with the coresponding atom in vals.
//	Returns a list.	
//	body is the procedure list as defined by the user.	
//	make hash of args and vals ["name", "val"]
	let hash = lamlisp.zip(args, vals);
	let test_fn = function (sexp) {
		console.log("test_fn call");
		console.log(sexp);		
		if(hash[sexp]) return hash[sexp];
		else return sexp;
		}
	return this.map(body, test_fn);
	}
