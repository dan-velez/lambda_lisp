/// minus sign is not seperator..
lamlisp.toks = function (exp) {
	let res = [];
	let buff = "";
	let add_buff = () => {
		if(buff.match(/.+/g)) res.push(buff);
		buff = "";
		}
	for(let i = 0; i < exp.length; i++) {
		if(exp[i].match(/[?a-z0-9.=:]/i)
		|| exp[i].match(this.lambda)) buff+=exp[i];
		else if(exp[i].match(/\s/)) add_buff();
		else if(exp[i].match(/[+\-\/*]/)) {
			add_buff();
//			lookahead for: ++ --  += -= \= *=	
			if(exp[i] == '+' && exp[i+1] == '+') {
				res.push("++"); ++i;}
			else if(exp[i] == '-' && exp[i+1] == '-') {
				res.push("--"); ++i;}
			else if(exp[i+1] == "=") {
				res.push(exp[i]+"="); ++i;}
			else res.push(exp[i]);
			}
		else if(exp[i].match(/[\^\:\$'\(\)]/)) {
			add_buff();
			res.push(exp[i]);
			}
		}
	console.log("tokens:");
	console.log(res);
	add_buff();
	return res;
	}

lamlisp.lex = function (exp) {
	let res = [];
	let toks = this.toks(exp);
	for(var i = 0; i < toks.length; i++) 
		if(toks[i].match(/^\d+(\.\d+)?$/))
			res.push(["num", parseFloat(toks[i])]);
		else if(toks[i].match(/\(/))
			res.push(["oparen", "("]);
		else if(toks[i].match(/\)/))
			res.push(["cparen", ")"]);
		else if(toks[i].match(/[\^?a-zA-Z0-9_=+\-\/*]/))
			res.push(["atom", toks[i]]);
		else if(toks[i].match(lamlisp.lambda))
			res.push(["atom", toks[i]]);
		else console.log("invalid token : " + toks[i]);
	console.log("lexical analysis:");
	console.log(res);
	return res;
	}

lamlisp.lex_match_paren = function (lexems) {
//	returns index of matching close paren
// 	of lexems stream
	let depth = 0;
	for(var i = 1; i < lexems.length; i++) {
		if(lexems[i][1] == "(") depth++;
		else if(lexems[i][1] == ")") depth--;
		if(depth == -1) return i+1;
		}
	return -1;
	} 
