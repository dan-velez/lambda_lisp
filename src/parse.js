lamlisp.parse = function (lexems) {
// creates an s-exp from >first< lexem token in lexems stream
	if(!lexems[0] || lexems[0][0] == "cparen") 
		return null;
	else if(lexems[0][0] == "oparen")
		return {
			car : this.parse(lexems.slice(
					1, lexems.length)),  // creates new stream
			cdr : this.parse(lexems.slice(
					this.lex_match_paren(lexems)
						,lexems.length))
			}
	else if(lexems[0][0] == "atom"
	|| lexems[0][0] == "num")
		return {
			car : lexems[0][1],
			cdr : this.parse(lexems.slice(
					1, lexems.length))
			}
	}
	
lamlisp.parse_str = function (str) {
	var lexems = this.lex(str);
	if(lexems[0][0] == "oparen")
		return this.parse(lexems.slice(
				1, lexems.length)) 
	else return this.parse(lexems);
	}

//console.log(
//	lamlisp.zip(lamlisp.parse_str("(1 2 3 4 5)"), 
//	lamlisp.parse_str("(a b (z y z) d e)" )));

/*let map_res = lamlisp.map(
		lamlisp.parse_str("(1 2 3 4)"),
		function (sexp) {
			console.log("in fn map call..input:");
//			this assumes each is an atom
			console.log(sexp);
			return sexp + 1;
			});
console.log("mapr res:");
console.log(map_res);*/

/*
console.log("sub args test");
let sub_args = lamlisp.sub_args(
	lamlisp.zip(
		lamlisp.parse_str("(x y z)"), // args
		lamlisp.parse_str("(3 4 5)")), // vals 
	lamlisp.parse_str("((+ x y z (z z z)))")  // body
	);
console.log("sub args res:");
console.log(sub_args);
*/
