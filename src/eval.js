lamlisp.eval = function (list) {	
	if(list == null) return null;
	else if(this.atomp(list)) return list;
	let res = {
		car:null, 
		cdr:null
		};
	if(!this.atomp(list.car))
		res.car = this.eval(list.car);
	else {
		for(let i = 0; i < this.fns.length; i++) {
			if(list.car == this.fns[i][0])
				return (this.fns[i][1]).call(
					lamlisp, list.cdr);

///				continue execution on rest of list


/*				res.car = (this.builtins[i][1]).call(
					lamlisp, list.cdr.car);
				if(list.cdr.cdr != null);
					res.cdr = this.eval(list.cdr.cdr);
				
				return res;	
// 					list.cdr.car
//					return*/
			}
		res.car = list.car;
		}

	if(list.cdr != null)
		res.cdr = this.eval(list.cdr);

	return res;
	}

lamlisp.eval_str = function (str) {
//	return this.eval(this.parse_str(str));
//////// dbg
	let list = this.parse_str(str);
	console.log("parsing result: ");
	console.log(list);
	let res = this.eval(list);
	console.log("eval result: ");
	console.log(res);
	return res;
	}

lamlisp.run = function (prog) {
	if(!prog) return "nil";
	prog = this.rm_comments(prog);
	if(this.syntax_check(prog)) // after each instruction.
		return this.list2sexp(
			this.eval_str(prog));
	else return "parenthesis mismatch";
	}

