<?php
include __DIR__."/../lib/site_tools.php";
$res = site_page("lambda lisp");

$textarea_style = "resize:horizontal;width:45%;";
$res = html_append_body($res,
"<script src=lamlisp_builtins.js></script>
<script src=lamlisp.js></script>
<script src=lamterm.js></script><hr/>");

$res = html_append_body($res,
sub_br(file_read(__DIR__."/readme")));

echo $res;

function sub_br ($str) {
// 	substitute any \n characters for <br/>
	$res = "";
	for($i = 0; $i < strlen($str); $i++)
		if($str[$i] == "\n") $res.="<br/>";
		else if($str[$i] == "\t") 
			$res.="&nbsp;&nbsp;&nbsp;&nbsp;";
		else $res.=$str[$i];
	return $res;
	}
