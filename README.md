# lambda lisp #

[http://dannybitbucket.blogspot.com/
](http://dannybitbucket.blogspot.com/)

Lambda lisp is an interpreter for a lisp type language implemented in Javascript. Currently it is a browser implementation. The goal of lambda lisp is to provide a concise and intuitive syntax which is based on symbols, such as the lambda symbol. All lambda lisp output is converted into a Javascript implementation of a list structure which looks like : 


```
#!javascript

car : 7,
cdr : {
    car : 8,
    cdr : {
        car : 9,
        cdr : null
        }
    }
}
```

## Usage ##

`&lambda;`

## Structure ##

Lambda lisp contains a terminal for writing lambda lisp code. This is implemented because the lambda lisp syntax utilizes the lambda symbol, which is not on most keyboards. To implement this, the terminal can write a lambda symbol using the backslash key. In addition, the terminal provides the ability to type parentheses without the use of the shift key, since lisp code utilizes parentheses a lot.

## Screenshot ##
![llisp2.png](https://bitbucket.org/repo/9anrnq/images/2132869355-llisp2.png)